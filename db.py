import sqlite3
import random

c = None
cur = None

def disconnect():
	c.close()

def reconnect():
	global c, cur
	
	c = sqlite3.connect('database.db',
		isolation_level=None, # Auto-commit
		detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES) # Try to guess types for variables
	c.row_factory = sqlite3.Row # Use nicer rows
	c.execute("PRAGMA foreign_keys = ON;") # Allow foreign keys
	cur = c.cursor()
	
	c.create_function("DRAND", 0, special_random) # Allow use of the seeded RNG

def special_random():
	return random.getrandbits(32) # Seeded RNG

def seedrandom(s):
	random.seed(s)

reconnect()