import fig
# EXPLANATION - pyfiglet acts strangely when embedded into executables, so figletgen.py makes a hardcoded version of all the strings I need which are loaded as a dictionary from fig.py

def pause():
	input('[-|-]')

def textraw(t):
	if t in fig.d:
		return fig.d[t]
	else:
		return '***'+str(t)+'***' # Fallback

def text(t):
	print(textraw(t))

def consent():
	s = None
	while not s:
		s = input().lower()
	return s=='yes' or s=='y' or s=='1'