import csv
import inflect
p = inflect.engine()
import shutil
import sqlite3

import db
import util

valid = ( 'Feed', 'Patrol', 'Site Claim', 'Site Build', 'Site Takeover', 'Site Audit', 'Site Nuke', 'Site Defend', 'Blood Build', 'Blood Attack', 'Haven Hunt', 'Custom', 'Spend', 'Sheriff Patrol', 'Blood Affect' )

def loadseed():
	with open('data/seed.txt', 'r') as f: # Seed the RNG with the value in seed.txt
		db.seedrandom(next(f))

def loadfeeding():
	with open('data/feeding.csv', 'r', newline='', encoding='utf-8') as f:
		get = csv.reader(f)
		errors = 0
		
		for linenum, line in enumerate(get):
			if not line or (linenum == 0 and line[0].lower() == 'who'): continue # Ignore headers and blanks
			
			# actor, location, amount, stealth, detail
			
			line = [(val if val != '' else None) for val in line]
			
			try:
				db.c.execute("INSERT INTO Feeding (actor, location, amount, stealth, detail) VALUES (?,?,?,?,?)", tuple(line))
			except sqlite3.IntegrityError as e:
				print('Error in feeding on line {}'.format(linenum+1))

def clearactions():
	shutil.copy('database.db', 'database.bak.db') # Back up the database just in case anything happens
	db.c.execute("DELETE FROM Actions") # Wipe out the actions table
	db.c.execute("INSERT INTO Actions (actor, for, location, success, stealth, type, detail) SELECT actor, actor, location, amount, stealth, 'Feed', detail FROM Feeding") # Copy the feeding data over
	db.c.execute("DELETE FROM Feeding") # Wipe out the feeding table

def loadactions():
	
#	db.c.execute("UPDATE Feeding SET taken=0") # Clear out the 'taken' column
	
	with open('data/actions.csv', 'r', newline='', encoding='utf-8') as f:
		get = csv.reader(f)
		errors = 0
		
		for linenum, line in enumerate(get):
			if not line or (linenum == 0 and line[0].lower() == 'who'): continue # Ignore headers and blanks
			
			# actor, benefit, action, regency, success, stealth, detail, money, vitae, wp, layer
			
			# Convert empty strings to None for database NULLs
			line = [(val if val != '' else None) for val in line]
			
			# Fix up numbers if necessary in the Money column
			try:
				line[7] = float(line[7].lstrip('$').replace(',','')) if line[7] else None # Remove commas and leading dollar sign; parse as float. If already NULL don't bother.
			except ValueError as e:
				print('Error on line {}: money expenditure "{}" could not be parsed'.format(linenum+1, line[7]))
			
			if not line[1]: # No 'for'
				line[1] = line[0]
			
			if line[2] not in valid:
				print('\tError on line {}: action type "{}" not recognized'.format(linenum+1, line[2]))
				errors += 1
				continue
			
			if line[2] == 'Site Nuke' or line[2] == 'Site Audit': # Special pre-validation rules, so that these don't appear in Patrols if invalid
				if not util.isregent(line[1], line[3]) and not util.isprince(line[1]):
					print('\tError on line {}: {} is not in control of {}, and thus cannot nuke'.format(linenum+1, line[1], line[3]))
					errors += 1
					continue
			
	#		if line[2] == 'Feed': # Mark total blood taken from a regency
	#			try:
	#				db.c.execute("UPDATE Regencies SET taken=taken+? WHERE name=?", (line[4], line[3]))
	#			except sqlite3.Error as e:
	#				print('\tError on line {}: was blood taken from "{}"?'.format(linenum+1, line[3]))
	#				errors += 1
	#				continue
			
			try:
				db.c.execute("INSERT INTO Actions (actor, for, type, location, success, stealth, detail, money, vitae, wp, layer) VALUES (?,?,?,?,?,?,?,?,?,?,?)", tuple(line))
			except sqlite3.IntegrityError as e:
				print('\tError in actions on line {}: '.format(linenum+1), end='')
				if not util.ischaracter(line[1]):
					print('"{}" is not in the database'.format(line[1]))
				else:
					print('is "{}" incorrect?'.format(line[3]))
				errors += 1
				continue
			
	db.c.execute("UPDATE Actions SET money=0 WHERE money IS NULL") # Get rid of NULLs
	db.c.execute("UPDATE Actions SET vitae=0 WHERE vitae IS NULL")
	db.c.execute("UPDATE Actions SET wp=0 WHERE wp IS NULL")
	db.c.execute("UPDATE Actions SET layer=0 WHERE layer IS NULL")
		
	print('Finished with', p.no('error',errors), 'passed')

def run():
	loadseed()
	loadfeeding()
	clearactions()
	loadactions()
