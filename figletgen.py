# Explanation:
# PyFiglet has a strange way of loading its fonts which prevents it from being embedded in an executable.
# Since I want an executable, this will not do.
# So this code takes all the figlets I need, makes a dictionary of them, and hardcodes it into a new module called "fig".

from pyfiglet import Figlet
f = Figlet(font='epic')

dic = {t : f.renderText(t) for t in ('PRAGMA', 'Data', 'Blood', 'Sites', 'Patrol', 'Spends', 'Import', 'Export', 'Backup', 'Restore')}

with open('fig.py', 'w') as file:
	file.write('# Automatically generated - see figletgen.py for details\n')
	file.write('d = ')
	file.write(str(dic))