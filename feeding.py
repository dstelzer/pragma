import db

def newvamp():
	print('New vampire?')
	name = input('Name: ')
	clan = input('Clan: ')
	cov = input('Covenant: ')
	db.c.execute("INSERT INTO Characters (name, clan, covenant, status) VALUES ?,?,?,?", (name, clan, cov, 0))
	print('Recorded.')

def status():
	name = input('Whose status changed? ')
	new = input('New status? ')
	db.c.execute("UPDATE Characters SET status=? WHERE name=?", (new, name))
	print('Adjusted.')

def feeding():
	name = input('Who? ')
	place = input('Where? ')
	amount = input('How much? ')
	stealth = input('Stealth? (-1 for not hiding) ')
	db.c.execute("INSERT INTO Feeding VALUES ?,?,?,?", (name, place, amount, stealth))
	print('Recorded.')