import sqlite3
import os
import csv

# if os.path.isfile('database.db'):
	# print('Deleting old database')
	# os.remove('database.db')

import db

creations = [
	"CREATE TABLE Positions (name TEXT PRIMARY KEY NOT NULL)",
	
#	"CREATE TABLE Clans (name TEXT PRIMARY KEY NOT NULL)",
#	"CREATE TABLE Covenants(name TEXT PRIMARY KEY NOT NULL)",
	("CREATE TABLE Characters (name TEXT PRIMARY KEY NOT NULL,"
		"status INT NOT NULL,"
	#	"clan REFERENCES Clans(name),"
	#	"covenant REFERENCES Covenants(name) DEFAULT ('Unaligned') ,"
		"position REFERENCES Positions(name))"),
	
	"CREATE TABLE Regencies (name TEXT PRIMARY KEY NOT NULL, blood INT NOT NULL, regent REFERENCES Characters(name), previous INT, carry INT DEFAULT 0, coordx INT, coordy INT)",
	"CREATE TABLE Paths (source REFERENCES Regencies(name), dest REFERENCES Regencies(name))",
	"CREATE TABLE Quadrants (name TEXT NOT NULL, contains REFERENCES Regencies(name))",
	
	"CREATE TABLE Sites (name TEXT PRIMARY KEY NOT NULL, location REFERENCES Regencies(name), owner REFERENCES Characters(name), level INT NOT NULL, influence TEXT)",
	
	"CREATE TABLE Actions (actor TEXT, for REFERENCES Characters(name), type TEXT, success INT, stealth INT DEFAULT -1, location REFERENCES Regencies(name), detail TEXT, money INT DEFAULT 0, vitae INT DEFAULT 0, wp INT DEFAULT 0, layer INT DEFAULT 0)",
	
	"CREATE TABLE Pending (actor TEXT REFERENCES Characters(name), type TEXT, target TEXT REFERENCES Sites(name), success INT DEFAULT 0, result TEXT, sheet TEXT, touched INT DEFAULT 0)",
	"CREATE TABLE Feeding (actor TEXT REFERENCES Characters(name), location REFERENCES Regencies(name), amount INT, stealth INT DEFAULT -1, detail TEXT)"
]

positions = [
	('Prince',),
	('Sheriff',)
]

#clans = [
#	('Daeva',),
#	('Gangrel',),
#	('Mekhet',),
#	('Nosferatu',),
#	('Ventrue',)
#]

#covenants = [
#	('Carthian',),
#	('Crone',),
#	('Invictus',),
#	('Lance',),
#	('Ordo',),
#	('Unaligned',)
#]

def getfromfile(fn):
	holder = []
	with open(fn, 'r', newline='', encoding='utf-8') as cfile:
		read = csv.reader(cfile, 'excel')
		for row in read:
			vals = tuple(i if i else None for i in row)
			holder.append(vals)
	return holder

def main():
	print('Creating tables')
	for num, line in enumerate(creations):
		try:
			db.c.execute(line)
		except sqlite3.Error as e:
			print('Error in creation {}: {}'.format(num+1, e))

	print('Inserting hardcoded values')
	try:
		db.c.executemany('INSERT INTO Positions VALUES (?)', positions)
	except sqlite3.IntegrityError:
		print('Error in Positions: duplicate values found')
#	db.c.executemany('INSERT INTO Clans VALUES (?)', clans)
#	db.c.executemany('INSERT INTO Covenants VALUES (?)', covenants)

#	print('Loading character file')
#	chars = getfromfile('characters.csv')
#	print('Found {} characters, inserting'.format(len(chars)))
#	db.c.executemany('INSERT INTO Characters VALUES (?,?,?,?,?)', chars)

#	print('Loading regency file')
#	reg = getfromfile('in/regencies.csv')
#	print('Found {} regencies, inserting'.format(len(reg)))
#	db.c.executemany('INSERT INTO Regencies (name,blood,regent) VALUES (?,?,?)', reg)

#	print('Loading path file')
#	paths = getfromfile('in/paths.csv')
#	print('Found {} paths, inserting'.format(len(paths)))
#	db.c.executemany('INSERT INTO Paths VALUES (?,?)', paths)
#	print('Inserting again in reverse')
#	rev = [t[::-1] for t in paths]
#	db.c.executemany('INSERT INTO Paths VALUES (?,?)', rev)

#	print('Loading site file')
#	sites = getfromfile('sites.csv')
#	print('Found {} sites, inserting'.format(len(sites)))
#	for i, site in enumerate(sites):
#		try:
#			db.c.execute('INSERT INTO Sites VALUES (?,?,?,?,?)', site)
#		except sqlite3.IntegrityError as e:
#			print('ERROR at {}: {}'.format(i+1, e))

	print('Done!! Database set up')
