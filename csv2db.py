import db
import csv
import sqlite3

import big

import makedb

def errmsg(msg, table, errs):
	
	if 'foreign' not in msg.lower(): # Primary key constraint
		print('It looks like the same name is being used more than once. Make sure every value in the first column is unique?')
		return
	
	if table == 'Characters':
	#	if errs >= 10:
	#		print("I'm guessing something is wrong with the Covenants. Check whether the entries in Characters and Covenants line up?")
	#	else:
		print('Something might be wrong with the Positions. Right now, only {} are important; all the rest should be blank.'.format(' and '.join(i[0] for i in makedb.positions)))
	elif table == 'Regencies':
		print('Every Regency needs either a current character as Regent, or an empty cell.')
	elif table == 'Sites':
		print('Most likely, a character was removed from the Status List, but they still have Sites. You can blank out their name to leave the Site behind, or delete the row entirely.')
	elif table == 'Pending':
		print('If a character is no longer on the Status List, their pending actions should be wiped out. Keeps things less cluttered.')
	else:
		print("I'm not sure quite what went wrong. Sorry, you're on your own here.")

def readtable(table):
	with open('csv/{}.csv'.format(table), 'r', newline='', encoding='utf-8') as f:
		read = csv.reader(f)
		headers = next(read) # Eat the first row
		
		cs = ','.join(headers)
		qs = ','.join('?' for _ in headers) # A ? for each value to insert
		
		ln = 2 # Number from 1 for non-coders, add 1 for headers
		errs = []
		msg = None
		
		for i, row in enumerate(read):
			data = [None if v=='' else v for v in row] # Replace empty strings with NULL; pass other falsy values as they are
			try:
				db.cur.execute('INSERT INTO {} ({}) VALUES ({})'.format(table, cs, qs), data)
			except sqlite3.IntegrityError as e:
				if msg is None: msg = str(e)
				errs.append(i)
		
		if errs:
			print("Problem! There's a reference to another table, but it didn't quite line up.")
			print('This happened on row{} {}'.format('s' if len(errs)==1 else '', ', '.join(str(e+2) for e in errs)))
			errmsg(msg, table, errs)
			return False
		
		return True

def main():
	big.text('Import')
	
#	tables = [row['name'] for row in db.c.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name")]
	# This sadly must be hardcoded to get the order right on the foreign key constraints... :/
	tables = ('Positions', 'Characters', 'Regencies', 'Quadrants', 'Paths', 'Sites', 'Actions', 'Pending', 'Feeding')
	
	for table in reversed(tables):
		db.c.execute("DELETE FROM {}".format(table)) # Wipe it out
	
	with db.c:
		for table in tables:
			print('Reading {} table...'.format(table))
			if not readtable(table): # Fill it in again
				print("This will cause more problems down the line, so I'm going to stop now. Run IMPORT again after editing the tables.")
				return
	
	db.c.execute('VACUUM') # Clear out the empty space if necessary
	
	print('Done!')

if __name__ == '__main__':
	main()
