import itertools
import operator
import csv
import sqlite3

import db
import util
import big

# EXPERIMENTAL new option
extra_claim_difficulty = 1
extra_build_difficulty = 1

# **************** SITE AUDITS ****************

def audit(where):
	results = ['Audit of sites in {}'.format(where)]
	
	for row in db.c.execute('SELECT * FROM Sites WHERE location=? AND level<>-1', (where,)):
		inf = ' ({})'.format(row['influence']) if row['influence'] is not None else ''
		name = row['name']+inf
		owner = 'owned by {}'.format(row['owner']) if row['owner'] is not None else 'unowned'
		
		results.append(' - {}, {}'.format(name, owner))
	
	return results

def runaudit():
	print('\tProcessing site audits')
	audits = []
	
	for row in db.c.execute('SELECT * FROM Actions WHERE type="Site Audit"'):
		result = ['({} - {})'.format(row['actor'], row['location'])]
		
		if util.isregent(row['actor'], row['location']) or isprince(row['actor']):
			result.extend(audit(row['location']))
		else:
			result.append('\tInsufficient authority; ask {}'.format(util.regentof(row['location'])))
		
		audits.append(result)
	
	with open('out/SiteAudits.txt', 'w', encoding='utf-8') as f:
		f.write(
			'\n\n'.join('\n'.join(line for line in acts) for acts in audits)
		)

# **************** UTILITY SECTION ****************

def ispending(who, action, what):
	return bool(db.c.execute('SELECT count(*) FROM Pending WHERE actor=? AND type=? AND target=?', (who, action, what)).fetchone()[0]) # Does a row exist in the database?

def ensurepending(who, action, what): # Check if a row exists for this action, and create it if it doesn't
	if who is None: who = util.owner(what)
	if not ispending(who, action, what):
		db.c.execute('INSERT INTO Pending (actor, type, target) VALUES (?,?,?)', (who, action, what))

def addsuccess(who, action, what, success): # Increase the successes on this action
	ensurepending(who, action, what)
	db.c.execute('UPDATE Pending SET success=success+?, touched=1 WHERE actor=? AND type=? AND target=?', (success, who, action, what))

def defend(what, success): # Decrease successes
	db.c.execute("UPDATE Pending SET success=success-?, touched=1 WHERE type='Takeover' AND target=?", (success, what))

def addallsuccess(action):
	print('\t\tAdding '+action+' results')
	for row in list(db.c.execute('SELECT for, detail, success FROM Actions WHERE type=?', ('Site '+action,))):
		try:
			addsuccess(row['for'], action, row['detail'], row['success'])
		except sqlite3.IntegrityError as e:
			print('Error with "{}": {}'.format(row['detail'], e))

def locof(site):
	place = None
	for row in db.c.execute('SELECT location FROM Sites WHERE name=?', (site,)):
		place = row[0]
		break
	return place

def matches(site, reg):
	return reg == locof(site)

# **************** MINI ROUTINES ****************

def newsites(): # Add new sites for Claims
	for row in list(db.c.execute("SELECT location, detail FROM Actions WHERE type='Site Claim'")):
		exists = next(db.c.execute("SELECT COUNT(*) FROM Sites WHERE name=?", (row['detail'],)))[0]
		if not exists:
			print('Unknown site: {}, {}. Create? '.format(row['detail'], row['location']), end='')
			if big.consent():
				db.c.execute("INSERT INTO Sites (name, location, level) VALUES (?,?,0)", (row['detail'], row['location']))

def copydata():
	print('\tCleaning and copying to scratch')
	db.c.execute('DELETE FROM Pending WHERE touched=-1 OR touched=2') # Clear out failed and finished ones
	db.c.execute("UPDATE Pending SET touched=0,sheet=NULL") # Make all others "untouched"
	newsites() # Mark new sites for Claim
	for action in ('Defend', 'Claim', 'Build', 'Takeover', 'Nuke'):
		addallsuccess(action)

def validate(type):
	print('\tValidating {} actions'.format(type))
	
	for row in list(db.c.execute("SELECT actor, rowid, location, detail FROM Actions WHERE type=?", ('Site '+type,))): # Make sure actions are happening in the right place!
		real = locof(row['detail'])
		if not real == row['location']:
			print('\tWarning: {} attempted to {} {} in {}, not {}. Corrected.'.format(row['actor'], type, row['detail'], row['location'], real))
			db.c.execute("UPDATE Actions SET location=? WHERE rowid=?", (real, row['rowid']))
	
	if type == 'Claim':
		for row in list(db.c.execute("SELECT target FROM Pending WHERE type='Claim' AND touched=1")): # Make sure Claims are valid
			if util.owner(row['target']) is not None: # If it's already owned
				db.c.execute("UPDATE Pending SET touched=-1, result='Already owned' WHERE type='Claim' AND target=?", (row['target'],))
			
	elif type == 'Build':
		for row in list(db.c.execute("SELECT rowid, actor, target FROM Pending WHERE type='Build' AND touched=1")): # Make sure Builds are valid
			if not util.owns(row['actor'], row['target']):
				db.c.execute("UPDATE Pending SET touched=-1, result='Not owned' WHERE rowid=?", (row['rowid'],))
			
	elif type == 'Takeover':
		for row in list(db.c.execute("SELECT rowid, actor, target FROM Pending WHERE type='Takeover' AND touched=1")): # Make sure Takeovers are valid
			if util.owns(row['actor'], row['target']) and util.influence(row['target']) is None:
				db.c.execute("UPDATE Pending SET touched=-1, result='Redundant' WHERE rowid=?", (row['rowid'],))
			
	elif type == 'Nuke':
		for row in list(db.c.execute("SELECT Pending.rowid, Pending.actor, Sites.location FROM Pending JOIN Sites ON Pending.target=Sites.name WHERE type='Nuke' AND touched=1")): # Make sure Nukes are valid
			if not util.isregent(row['actor'], row['location']) and not util.isprince(row['actor']):
				db.c.execute("UPDATE Pending SET touched=-1, result='Unauthorized' WHERE rowid=?", (row['rowid'],))

def breakties(type):
	print('\t\tFinding and resolving ties')
	
	groups = itertools.groupby(
		db.c.execute("SELECT Pending.rowid AS id, Pending.actor AS actor, Pending.target AS target, Characters.status AS status FROM Pending JOIN Characters ON Pending.actor=Characters.name WHERE (Pending.type=?) AND Pending.touched=2 ORDER BY target ASC, status DESC, DRAND()", (type,)),
		operator.itemgetter('target')) # Select all cases where multiple characters successfully claimed or took over the same site. Sort them first by which site was taken (so Python can divide them up), then by higher status, then if that fails by coin flip. Pass the set of results to GroupBy to break them into lists based on which site is in question.
	
	for key, group in groups: # Now go through and consider each site in contention.
		winner = next(group) # The first one will be allowed to succeed
		print('\t\t\t{} goes to {}'.format(key, winner['actor']))
		
		for val in group: # But void the rest
			db.c.execute("UPDATE Pending SET touched=-1,result='Tied',sheet=NULL WHERE rowid=?", (val['id'],))

def sitewars():
	print('\tEvaluating defense and takeovers')
	
	for defense in list(db.c.execute("SELECT target, success FROM Pending WHERE type='Defend'")):
		defend(defense['target'], defense['success'])
	
	validate('Takeover')
	
	actions = list(db.c.execute("SELECT Pending.rowid AS id, Pending.actor AS actor, Pending.target AS target, Pending.success AS success, Pending.touched AS flag, Sites.level*4 AS goal, Sites.owner AS prev, Sites.location AS loc, Sites.influence AS influence FROM Pending JOIN Sites ON Pending.target=Sites.name WHERE Pending.type='Takeover' AND Pending.touched<>-1")) # Make in-memory list to allow other queries in the meantime
	
	for action in actions: # Can't do this directly with an UPDATE since it pulls from multiple tables :/ need to understand JOINs better...
		result = '{}/{}'.format(action['success'], action['goal'])
		sheet = None
		
		if action['success'] >= action['goal']:			
			sheet = 'Take from {}, give to {}, in {}'.format(action['prev'] if action['prev'] is not None else 'nobody', action['actor'], action['loc'])
			flag = 2
		elif action['success'] < 0:
			flag = -1
		else:
			flag = action['flag']
		
		db.c.execute("UPDATE Pending SET touched=?,result=?,sheet=? WHERE rowid=?", (flag, result, sheet, action['id']))
	
	breakties('Takeover') # Just in case of ties
	
	# Now transfer Influence
	
	actions = list(db.c.execute("SELECT Pending.actor AS actor, Pending.target AS target, Sites.influence AS influence, Sites.owner AS oldowner, Pending.rowid AS id FROM Pending JOIN Sites ON Pending.target=Sites.name WHERE Pending.type='Takeover' AND Pending.touched=2 AND Sites.influence IS NOT NULL")) # Now take just the successful ones involving Influence
	
	print('\t{} influential takeovers'.format(len(actions)))
	
	for action in actions: # Handle these specially if the player chooses
		print('\t{} took over {} ({}). Should {} keep the Site itself? '.format(action['actor'], action['target'], action['influence'], action['oldowner']), end='')
		if big.consent():
			
			successful = False
			while not successful: # Repeat this part until the site is valid
				new = input('\t\tNew influence: ')
				successful = util.exists(new) and util.influence(new) is None
				if not successful:
					print('\t\tThat is not a valid Site.')
			
			sheet = '{} gained {}'.format(new, action['influence'])
			db.c.execute("UPDATE Pending SET touched=3, sheet=? WHERE rowid=?", (sheet, action['id'])) # Don't resolve this as a normal Takeover
			db.c.execute("UPDATE Sites SET influence=NULL WHERE name=?", (action['target'],))
			db.c.execute("UPDATE Sites SET influence=? WHERE name=?", (action['influence'], new))
	
	# Now, all that's left are successful, non-tied, direct, Site Takeovers.
	actions = list(db.c.execute("SELECT actor, target FROM Pending WHERE type='Takeover' AND touched=2"))
	for action in actions: # Time to alter the Sites table itself
		db.c.execute("UPDATE Sites SET level=1,owner=? WHERE name=?", (action['actor'], action['target']))
	
	# Now, check Builds one more time
	db.c.execute("UPDATE Pending SET touched=1, result=NULL WHERE touched=-1") # Flag failed ones to run again
	makesites(onlybuild=True)
	# Just in case someone took over and then wanted to build it up

def pendclaims(): # Explain why claims failed
	target = 1 + extra_claim_difficulty
	for row in list(db.c.execute("SELECT success, rowid AS id FROM Pending WHERE type='Claim' AND touched=1 AND success<?", (target,))): # Get the failures
		text = '{}/{} to claim'.format(row['success'], target)
		db.c.execute("UPDATE Pending SET result=? WHERE rowid=?", (text, row['id']))

def makesites(onlybuild=False):
	print('\tEvaluating claims and builds', '(again)' if onlybuild else '')
	
	validate('Build')
	
	if not onlybuild:
		db.c.execute("UPDATE Pending SET touched=2 WHERE type='Claim' AND touched=1 AND success>=?", (1 + extra_claim_difficulty ,)) # Set all as successful to start out
		pendclaims()
		breakties('Claim') # Then void out duplicates
		
		validate('Build')
	
	for row in list(db.c.execute("SELECT Pending.rowid AS id, Pending.actor AS actor, Pending.target AS target, Sites.location AS loc FROM Pending INNER JOIN Sites ON Pending.target=Sites.name WHERE Pending.type='Claim' AND Pending.touched=2")):
		db.c.execute("UPDATE Sites SET level=1,owner=? WHERE name=?", (row['actor'], row['target'])) # Change in Sites database
		db.c.execute("UPDATE Pending SET sheet=? WHERE rowid=?", ('Claimed ({})'.format(row['loc']), row['id'])) # Make note to STs
	
	for row in list(db.c.execute("SELECT Pending.target AS place, Pending.success AS success, Pending.rowid AS id, Sites.level AS level FROM Pending JOIN Sites ON Pending.target=Sites.name WHERE Pending.type='Build' AND Pending.touched=1")):
		succ = row['success']
		goal = row['level'] + 1 + extra_build_difficulty
		where = row['place']
		builds = 0
		
		while succ >= goal:
			succ -= goal
			goal += 1
			builds += 1
		
		newlevel = row['level']+builds
		
		flag = 1 if succ else -1 # Change the flag to "done" if there are no carry-over successes
		text1 = '+{} (at {})'.format(builds, newlevel) # List both amount of increase and total level for STs
		text2 = '{}/{} for next'.format(succ, goal) if succ else None
		
		db.c.execute("UPDATE Pending SET success=?,sheet=?,result=?,touched=? WHERE rowid=?", (succ, text1, text2, flag, row['id'])) # Feedback
		db.c.execute("UPDATE Sites SET level=? WHERE name=?", (newlevel, where)) # Internal records

def nukes():
	print('\tEvaluating nukes')
	
	validate('Nuke')
	
	for row in list(db.c.execute("SELECT Pending.rowid AS id, Sites.owner AS owner, Pending.target AS place FROM Pending JOIN Sites ON Pending.target=Sites.name WHERE Pending.type='Nuke' AND Pending.touched=1")):
		note = 'Destroyed ({})'.format(row['owner'])
		db.c.execute("UPDATE Pending SET sheet=?,touched=2 WHERE rowid=?", (note, row['id'])) # Feedback
		db.c.execute("UPDATE Sites SET level=0,owner=NULL WHERE name=?", (row['place'],)) # Internal records

def summarize():
	print('\tSummarizing')
	
	with open('out/SiteActions.csv', 'w', newline='', encoding='utf-8') as f1:
		out = csv.writer(f1)
		out.writerow(('Character', 'Action', 'Site', 'Result', 'Sheet Change'))
		
		for row in db.c.execute("SELECT actor, type, target, result, sheet FROM Pending WHERE (result IS NOT NULL OR sheet IS NOT NULL) AND touched!=0 ORDER BY actor ASC, target ASC"):
			out.writerow(row)
	
	with open('out/Influence.csv', 'w', newline='', encoding='utf-8') as f2:
		out = csv.writer(f2)
		out.writerow(('Influence', 'Name', 'Regency'))
		
		for row in db.c.execute("SELECT influence, name, location FROM Sites WHERE influence IS NOT NULL ORDER BY influence ASC"):
			out.writerow(row)

# **************** PRIMARY ROUTINE ****************

def run():
	print('Processing Sites')
	
	copydata() # Copy Actions to Pending
#	validate() # Make sure all actions are legal, mark as -1 if not
#	This is now split into several smaller validation subroutines, called by the individual actions
	
	makesites() # Claim and Build
	sitewars() # Defend and Takeover
	nukes() # Nuke
	runaudit() # Site Audits
	
	summarize()
