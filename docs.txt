          _______  _______  _______  _______          _________ ______   _______ 
|\     /|(  ____ \(  ____ \(  ____ )(  ____ \|\     /|\__   __/(  __  \ (  ____ \
| )   ( || (    \/| (    \/| (    )|| (    \/| )   ( |   ) (   | (  \  )| (    \/
| |   | || (_____ | (__    | (____)|| |      | |   | |   | |   | |   ) || (__    
| |   | |(_____  )|  __)   |     __)| | ____ | |   | |   | |   | |   | ||  __)   
| |   | |      ) || (      | (\ (   | | \_  )| |   | |   | |   | |   ) || (      
| (___) |/\____) || (____/\| ) \ \__| (___) || (___) |___) (___| (__/  )| (____/\
(_______)\_______)(_______/|/   \__/(_______)(_______)\_______/(______/ (_______/

==================================================================================

First steps: to run Pragma, you'll need Python 3.5 or higher, the sqlite3 module
(included by default), and the cursesmenu module (if you want fancy menus).

To run it, extract all the files to some directory, open a terminal, and run
	>python3 pragma.py
This should open the main menu. Use the arrow keys (if you have cursesmenu) or
type a number, then press ENTER to carry out that action.

The usual workflow is:
 1 - Backup
 2 - Restore
 3 - Edit actions.csv, feeding.csv, and seed.txt (date of the next game)
 4 - Action Processing
 5 - Go back to step 2 as needed

 _______  _______ __________________ _______  _       
(  ___  )(  ____ \\__   __/\__   __/(  ___  )( (    /|
| (   ) || (    \/   ) (      ) (   | (   ) ||  \  ( |
| (___) || |         | |      | |   | |   | ||   \ | |
|  ___  || |         | |      | |   | |   | || (\ \) |
| (   ) || |         | |      | |   | |   | || | \   |
| )   ( || (____/\   | |   ___) (___| (___) || )  \  |
|/     \|(_______/   )_(   \_______/(_______)|/    )_)
=======================================================

Action Processing is the main part of Pragma: read in actions from data/actions.csv,
handle them, and write out the results.

Every line of actions.csv should contain:
 - The person doing the action (the name of the Retainer, not the regnant)
 - The person benefitting from the action (e.g. who should get the Site?)
 - The type of action
 - The regency affected
 - How many successes
 - How many stealth successes (or -1 for blatant, or -9 for "omit from patrols")
 - Action-specific "details" (see below)
 - Money expenditures
 - Vitae expenditures
 - Willpower expenditures
 - The "layer" of the action (0 normally, 1 for Twilight, 2 for Shadow...)

The specifics depend on the type of action.

"Feed" - Success is the amount of blood taken. Detail is the game date.

"Patrol" - Detail is a number representing which layer is seen. Layer 0 means they
	can only see things in the physical world, like Allies would. Layer 1 means they
	see into the Twilight as well, like with Auspex 1. Layer -1 means they see *only*
	the Twilight and not the real world. (Similarly for 2 and -2, the Shadow.)
	Success of -1 is a special value meaning they see everything. A patroller using
	Spirit Sight, for example, would have a success of -1 and a detail of -1.

"Sheriff Patrol" - As above, except that "East" or "West" replaces the regency.

"Site [Build, Claim, Defend, Nuke, Takeover]" - Detail is the name of the Site.

"Site [Audit, Build, Claim]" - Success is the number of actions put in. If a Site Build
	was expedited with money, increase Success appropriately.

"Blood [Build, Attack]" - Nothing special.

"Haven Hunt" - Detail is the target's name. Note that Pragma does not know where people's
	Havens are; she just makes sure it appears in Patrols. The actual Haven Raid details
	are up to the Storytellers.

"Spend" - This action does nothing. Really. But the money, Vitae, and WP spent on it
	will be recorded, to keep the records straight for game.

"Custom" - This action also does nothing. But based on its Stealth and Layer, it may be
	visible in patrols. Detail should be a partial sentence, which will be appended to
	the person's name when seen in patrols. "was digging in the Cemetary", for example.

If any errors turn up, Pragma will explain them, then try to ignore them and keep going.
If the problems are significant you should fix them, Restore, and Process again.

 ______   _______  _______  _                 _______ 
(  ___ \ (  ___  )(  ____ \| \    /\|\     /|(  ____ )
| (   ) )| (   ) || (    \/|  \  / /| )   ( || (    )|
| (__/ / | (___) || |      |  (_/ / | |   | || (____)|
|  __ (  |  ___  || |      |   _ (  | |   | ||  _____)
| (  \ \ | (   ) || |      |  ( \ \ | |   | || (      
| )___) )| )   ( || (____/\|  /  \ \| (___) || )      
|/ \___/ |/     \|(_______/|_/    \/(_______)|/
======================================================

Backup copies Pragma's database to a separate file, backup.db. Restore copies it back.
You usually run Backup after finishing last week's actions, then Restore as necessary
if anything goes wrong in this week's.

 _______  _______          
(  ____ \(  ____ \|\     /|
| (    \/| (    \/| )   ( |
| |      | (_____ | |   | |
| |      (_____  )( (   ) )
| |            ) | \ \_/ / 
| (____/\/\____) |  \   /  
(_______/\_______)   \_/
===========================

Export dumps all of the data from Pragma's database into the csv/ folder. These files can
be opened and edited with Excel or Calc; for example, the easiest way to deal with new
characters is to Export, then edit Characters.csv and add the new rows at the end. Import
takes all these files and attempts to put them back into the database. If any errors show
up, you should edit the files and Import again before trying to use anything else.

 _______  _______  _       
(  ____ \(  ___  )( \      
| (    \/| (   ) || (      
| (_____ | |   | || |      
(_____  )| |   | || |      
      ) || | /\| || |      
/\____) || (_\ \ || (____/\
\_______)(____\/_)(_______/
===========================

Don't use this unless you know what you're doing! This is the more direct way to change
anything in the database; it gives you a shell into the SQL layer. You can alter absolutely
anything through this option, but that also means you have the power to mess things up.
(Remember to end your commands with semicolons; the dialect is SQLite 3.)

 _______  _______  _        _______ _________ _______           _______ _________
(  ____ \(  ___  )( (    /|(  ____ \\__   __/(  ____ )|\     /|(  ____ \\__   __/
| (    \/| (   ) ||  \  ( || (    \/   ) (   | (    )|| )   ( || (    \/   ) (   
| |      | |   | ||   \ | || (_____    | |   | (____)|| |   | || |         | |   
| |      | |   | || (\ \) |(_____  )   | |   |     __)| |   | || |         | |   
| |      | |   | || | \   |      ) |   | |   | (\ (   | |   | || |         | |   
| (____/\| (___) || )  \  |/\____) |   | |   | ) \ \__| (___) || (____/\   | |   
(_______/(_______)|/    )_)\_______)   )_(   |/   \__/(_______)(_______/   )_(
=================================================================================

Hopefully you won't need this; it's provided in case things go horribly wrong. This option
creates a new and completely empty database from the schema in makedb.py. You can then fill
it in by using CSV import or through the SQL shell. If something is broken at a fundamental
level, run Backup, then Export, then Construct, then Import. If you want to burn everything
and start from scratch, run Backup, then Construct, then Export, and have at it.
