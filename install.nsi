; -------------------------------
; Start
 
 
  !define MUI_PRODUCT "Pragma"
  !define MUI_FILE "pragma"
  !define MUI_VERSION ""
  Name "Pragma"
  !define MUI_BRANDINGTEXT "Pragma"
  CRCCheck On
 
  !include "${NSISDIR}\Contrib\Modern UI\System.nsh"
 
 
;---------------------------------
;General
 
  OutFile "install.exe"
;  ShowInstDetails "nevershow"
;  ShowUninstDetails "nevershow"
  ;SetCompressor "bzip2"
 
;  !define MUI_ICON "icon.ico"
;  !define MUI_UNICON "icon.ico"
;  !define MUI_SPECIALBITMAP "Bitmap.bmp"
 
 
;--------------------------------
;Folder selection page
 
  InstallDir "$LOCALAPPDATA\${MUI_PRODUCT}"
 
 
;--------------------------------
;Modern UI Configuration
 
;  !define MUI_WELCOMEPAGE
;  !define MUI_DIRECTORYPAGE
;  !define MUI_ABORTWARNING
;  !define MUI_UNINSTALLER
;  !define MUI_UNCONFIRMPAGE
;  !define MUI_FINISHPAGE

Page directory
Page instfiles
 
 
;--------------------------------
;Language
 
  !insertmacro MUI_LANGUAGE "English"
 
 
;-------------------------------- 
;Modern UI System
 
;  !insertmacro MUI_SYSTEM 
 
 
;-------------------------------- 
;Installer Sections     
Section
 
;Add files
  SetOutPath "$INSTDIR"
  File "pragma.exe"
  SetOutPath "$INSTDIR\data"
  File "data\actions.csv"
  File "data\feeding.csv"
  File "data\seed.txt"
  File "data\blankmap.png"
  SetOutPath "$INSTDIR\data\fonts"
  File "data\fonts\italic.ttf"
  File "data\fonts\regular.ttf"
  SetOutPath "$INSTDIR\csv"
  File "csv\Actions.csv"
  File "csv\Characters.csv"
  File "csv\Feeding.csv"
  File "csv\Paths.csv"
  File "csv\Pending.csv"
  File "csv\Positions.csv"
  File "csv\Quadrants.csv"
  File "csv\Regencies.csv"
  File "csv\Sites.csv"
 
;create desktop shortcut
  CreateShortCut "$DESKTOP\${MUI_PRODUCT}.lnk" "$INSTDIR\pragma.exe"
  CreateShortCut "$DESKTOP\PragmaFiles.lnk" "$INSTDIR"
 
;create start-menu items
  CreateDirectory "$SMPROGRAMS\${MUI_PRODUCT}"
  CreateShortCut "$SMPROGRAMS\${MUI_PRODUCT}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\${MUI_PRODUCT}\${MUI_PRODUCT}.lnk" "$INSTDIR\${MUI_FILE}.exe" "" "$INSTDIR\${MUI_FILE}.exe" 0
 
;write uninstall information to the registry
;  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayName" "${MUI_PRODUCT} (remove only)"
;  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString" "$INSTDIR\Uninstall.exe"
 
;  WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
;Section "Uninstall"
 
;Delete Files 
;  RMDir /r "$INSTDIR\*.*"    
 
;Remove the installation directory
;  RMDir "$INSTDIR"
 
;Delete Start Menu Shortcuts
;  Delete "$DESKTOP\${MUI_PRODUCT}.lnk"
;  Delete "$DESKTOP\PragmaFiles.lnk"
;  Delete "$SMPROGRAMS\${MUI_PRODUCT}\*.*"
;  RmDir  "$SMPROGRAMS\${MUI_PRODUCT}"
 
;  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}"  
 
;SectionEnd
 
 
;--------------------------------    
;MessageBox Section
;Function that calls a messagebox when installation finished correctly
Function .onInstSuccess
  MessageBox MB_OK "You have successfully installed ${MUI_PRODUCT}. Use the desktop icon to start the program."
FunctionEnd
 
Function un.onUninstSuccess
  MessageBox MB_OK "You have successfully uninstalled ${MUI_PRODUCT}."
FunctionEnd