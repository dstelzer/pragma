import csv
from math import copysign

import db
import util
import bloodimg

# These are functions so they can be edited easily for a new blood system.

def requires(where): # How many successes does it take to affect this place?
#	supply = next(db.c.execute("SELECT blood FROM Regencies WHERE name=?", (where,)))['blood']
#	return 1 if supply < 15 else 2
	return 1 # New system is simpler, always 1

def maxblood(where): # What's the cap?
	return 20

def minblood(where): # What's the cut-off?
	return 0

def sendto(source):
	return next(db.c.execute("SELECT Regencies.name FROM Regencies INNER JOIN Paths ON Paths.dest=Regencies.name WHERE Paths.source=? AND Regencies.blood < ? ORDER BY DRAND() LIMIT 1", (source, maxblood(source))), (None,))[0] # Pick one adjacent valid regency at random. If there are none, return None.

def drawfrom(dest):
	return next(db.c.execute("SELECT Regencies.name FROM Regencies INNER JOIN Paths ON Paths.dest=Regencies.name WHERE Paths.source=? AND Regencies.blood > ? ORDER BY DRAND() LIMIT 1", (dest, minblood(dest))), (None,))[0] # Pick one adjacent valid regency at random. If there are none, return None.

def carry(where):
	return next(db.c.execute("SELECT carry FROM Regencies WHERE name=?", (where,)))['carry']

def blood(where):
	return next(db.c.execute("SELECT blood FROM Regencies WHERE name=?", (where,)))['blood']

def affectblood(where, amount):
	db.c.execute("UPDATE Regencies SET blood=blood+? WHERE name=?", (amount, where))

def affectcarry(where, amount):
	db.c.execute("UPDATE Regencies SET carry=carry+? WHERE name=?", (amount, where))

def copydata():
	db.c.execute("UPDATE Regencies SET previous = blood") # Copy the BLOOD column to PREVIOUS

def countwars():
	for row in list(db.c.execute("SELECT type, location, success FROM Actions WHERE type='Blood Build' OR type='Blood Attack'")): # Copy all the actions into a memory list so we can mess with the database in the meantime
		if row['type'] == 'Blood Build': # Now write the Carry values to the number of successes total
			affectcarry(row['location'], row['success'])
		elif row['type'] == 'Blood Attack':
			affectcarry(row['location'], -row['success'])
		# Put other hypothetical blood actions here

def affects():
	for row in list(db.c.execute("SELECT location, success FROM Actions WHERE type='Blood Affect'")):
		succ = row['success']
		inverse = False
		if succ < 0:
			inverse = True
			succ = -succ
		where = row['location']
		
		print('\t\tAttempting to {} {} at {}, which currently has {}'.format('remove' if inverse else 'add', succ, where, blood(where)))
		
		for i in range(succ):
			if inverse:
				if blood(where) > minblood(where): # This one can be drained
					affectblood(where, -1)
				else:
					dest = drawfrom(where)
					print('\t\t\tUnderflow: {}th draws from {}'.format(i, dest))
					affectblood(dest, -1)
			else:
				if blood(where) < maxblood(where): # This one can hold more
					affectblood(where, 1)
				else:
					dest = sendto(where)
					print('\t\t\tOverflow: {}th spills into {}'.format(i, dest))
					affectblood(dest, 1)

def process(where): # Affect the blood in the regency, returning True if it's been changed this round.
	# This could potentially be optimized a lot by removing all the requires() calls.
	# However, this model makes it much easier to use a system with variable success rates
	# such as the one used prior to Mar 2016.
	
	if not carry(where): return False # Don't bother doing anything for zero successes—shouldn't happen, but in case of bad data
	target = requires(where) # How many successes needed to move blood this iteration?
	sign = copysign(1, carry(where)) # Get the sign of the successes (i.e. is this build or attack?)
	touched = False # Nothing has changed yet
	
	while abs(carry(where)) >= target:
		if (sign>0 and blood(where) >= maxblood(where)) or (sign<0 and blood(where) <= minblood(where)): # No more to be done, the regency is full or empty already
			break
		
		dest = drawfrom(where) if sign>0 else sendto(where) # Find appropriate regencies to move through
		if dest is None: # Nothing can be done, no good targets adjacent
			break
		
		affectblood(where, sign) # Action (affect this regency)
		affectblood(dest, -sign) # Reaction (affect the adjacent one)
		affectcarry(where, -target*sign) # Reduce success total
		
		touched = True # Now we've done something in this iteration
		target = requires(where) # Update the target number since the blood total has changed - not strictly necessary any more, but important if using a system where successes-required can vary
	
	return touched

def resolve():
	changes = True # Make sure it runs at least once
	while changes: # Now repeat until nothing changes between runs
		order = list(i[0] for i in db.c.execute("SELECT name FROM Regencies WHERE carry<>0 ORDER BY blood ASC")) # Get the list of regencies, sorted from lowest blood to highest
		changes = sum(1 if process(where) else 0 for where in order) # Process in order, and count how many have been changed
		print('\t\t{} directly affected'.format(changes))
	
	db.c.execute("UPDATE Regencies SET carry=0 WHERE (carry>0 AND blood>=20) OR (carry<0 AND blood<=0)") # Clear out extra successes from capped regencies; these don't carry over

def export():
	with open('out/BloodWars.csv', 'w', newline='', encoding='utf-8') as f:
		out = csv.writer(f)
		out.writerow(('Regency', 'Regent', 'Old', 'Delta', 'New', 'Carry'))
		
		for row in db.c.execute("SELECT name, regent, previous, blood-previous, blood, carry FROM Regencies WHERE blood>0 OR previous>0 ORDER BY name"):
			out.writerow(row)

def run():
	print('Processing Blood Wars')
	
	copydata() # Move current blood totals over
	
	print('\tCounting successes')
	countwars()
	
	print('\tResolving actions')
	resolve()
	
	print('\tAffecting blood')
	affects()
	
	print('\tExporting results')
	export()
	
	bloodimg.run()
