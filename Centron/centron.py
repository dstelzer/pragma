import csv

data = {}

with open('feed.csv', 'r', newline='') as f1:
	read = csv.reader(f1)
	headers = next(read)
	
	for actor, location, amount, stealth in read:
		if location not in data:
			data[location] = []
		
		data[location].append((actor, amount, stealth))

print('Finished reading feed data')
print('Len: {}'.format(len(data)))

with open('patrol.csv', 'r', newline='') as f2:
	read = csv.reader(f2)
	headers = next(read)
	
	for patroller, location, success, ties in read:
		ties = bool(ties)
		success = int(success)
		print('\nFeeding in {}, for {} ({}{})'.format(location, patroller, success, ', regent' if ties else ''))
		
		found = False
		
		if location in data:
			for eater, amount, stealth in sorted(data[location]):
				stealth = int(stealth)
				
				if stealth > success or (stealth == success and not ties): continue
				
				found = True
				adverb = ' blatantly' if stealth < 0 else ''
				print(' - {}{} took {} blood'.format(eater, adverb, amount))
		
		if not found:
			print(' - No feeding apparent')
