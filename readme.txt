Windows Build Instructions:

Put all of the Python files on a Windows machine with py35
Run build.bat (or just pyinstaller -F pragma.py; xcopy dist/pragma.exe pragma.exe)
Run NSIS on install.nsi
If all works correctly, push install.exe
