# Make a pretty picture of the Blood Map

from PIL import Image, ImageFont, ImageDraw

import db
import big

fontsize = 32

def addlabel(drawer, pos, color, font, line):
	drawer.textsize(line, font)
	drawer.text(pos, line, font=font, fill=color)

def affect(image, labels):
	if image.mode != 'RGBA': image = image.convert('RGBA') # Ensure proper encoding
	drawer = ImageDraw.Draw(image)
	
#		fontpath = '/usr/share/fonts/truetype/hightower/'
	
	fonts = {} # Load the various fonts we'll be using
	fonts['regular'] = ImageFont.truetype('data/fonts/regular.ttf', int(fontsize*1.5))
	fonts['bold'] = ImageFont.truetype('data/fonts/regular.ttf', fontsize)
	fonts['italic'] = ImageFont.truetype('data/fonts/italic.ttf', fontsize)
	
	color = (0,0,0)
	
	for line, pos, mode in labels:
		addlabel(drawer, pos, color, fonts[mode], line)

def process():
	img = Image.open('data/blankmap.png')
	labels = []
	for row in db.c.execute("SELECT * FROM Regencies WHERE coordx IS NOT NULL AND coordy IS NOT NULL"):
		x, y = row['coordx'], row['coordy']
		name = row['name']
		regent = row['regent']
		blood = str(row['blood'])
		
		w = max(len(name), len(regent) if regent else 0, len(blood))
		
		if name: labels.append((name.center(w), (x, y), 'bold'))
		if regent: labels.append((regent.center(w), (x, y+fontsize), 'italic'))
		labels.append((blood.center(w), (x, y+fontsize*2), 'regular'))
	
	affect(img, labels)
	img.save('out/bloodmap.png')
	img.show()
	#Testing a push to the repo

def run():
	print('Generating map image')
	process()

def main():
	run()
	print('Done!')
	big.pause()

if __name__ == '__main__':
	run()
