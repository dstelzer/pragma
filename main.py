import newdata
import bloodwars
import sites
import patrols
import spends
import big

def main():	
	big.text('Data')
	newdata.run()
	big.pause()
	
	big.text('Blood')
	bloodwars.run()
	big.pause()
	
	big.text('Sites')
	sites.run()
	big.pause()
	
	big.text('Patrol')
	patrols.run()
	big.pause()
	
	big.text('Spends')
	spends.run()
	big.pause()
	
	print('Done!')

if __name__ == '__main__':
	main()