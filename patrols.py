import sqlite3
import inflect
p = inflect.engine()

import db
import util

def sheriffpatrol(where, who, success, stealth, layer):
	if not util.issheriff(who):
		print('\tError: {} is not Sheriff'.format(who))
		return
	places = list(db.c.execute("SELECT contains FROM Quadrants WHERE name=?", (where,)))
	if not places:
		print('\tError: {} patrolled {}, which is not a quadrant'.format(who, where))
		return
	
	for row in places:
		place = row['contains']
		try:
			db.c.execute("INSERT INTO Actions (actor, for, type, location, success, stealth, detail) VALUES (?,?,?,?,?,?,?)", (who, who, 'Patrol', place, success, stealth, layer))
		except sqlite3.IntegrityError:
			print('\tInternal error in Sheriff patrols; check your Quadrants table')
			print('\t\t{} patrolled {}'.format(who, place))

def patrol(where, who, success, name, layer):
	ties = util.isregent(who, where) or util.isprince(who) # Should they win ties on stealth?
	
	if layer is None or layer == '': layer = 0 # Make sure int() doesn't choke - assume Prime Material unless told otherwise
	layer = int(layer) # Coerce to int
	
	if layer < 0: # Negative means show ONLY this plane, positive means show this plane and all below
		layer = -layer
		exclusive = True
	else:
		exclusive = False
	
	if success == -1: # Special case
		successline = '(seeing everything)'
	else:
		successline = 'with ' + p.no('success', success)
	
	# Reporting things
	line = []
	line.append(name)
	if ties: line.append('(regent)')
	line.append('patrolled')
	if exclusive: # Only patrolling one specific layer
		if layer == 1: line.append('the Twilight in')
		elif layer == 2: line.append('the Shadow in')
	line.append(where)
	if who != name: line.append('for '+who)
	line.append(successline)
	
	bloodseen = {} # How much feeding was seen on each date?
	
	results = [ ' '.join(line)+':' ]
	
#	results = ['{}{} patrolled {}{} with {}:'.format(name, ' (regent)' if ties else '', where, ' for {}'.format(who) if who != name else '', p.no('success', success))] # Mae Lawrence (regent) patrolled Savoy for Kyle Jennings with 3 successes:
	
	for row in db.c.execute("SELECT * FROM Actions WHERE location=? AND (layer<=? OR layer IS NULL) ORDER BY (CASE WHEN type='Feed' THEN 1 ELSE 0 END) DESC, layer ASC, type ASC, detail ASC, actor ASC, success DESC", (where, layer)): # Feeding first, then sort by layer, type, target, actor, successes
		act = row['type']
		stealth = row['stealth'] if row['stealth'] is not None else -1 # In case of bad data
		person = row['actor']
		plane = row['layer'] if row['layer'] is not None else 0 # Assume Prime Material again if necessary
		manner = '' # Optional adverb
		
		if plane != layer and exclusive: continue # We didn't see this one because planar restrictions
		
		if stealth == -9: continue # This action does not actually exist here
		
		if act == 'Feed' and row['detail'] not in bloodseen: bloodseen[row['detail']] = 0 # There was feeding here on this date
		
		if success != -1: # This isn't see-everything wooj
			if stealth > success or (stealth == success and not ties):
				if row['for'] == who:
					manner = ' stealthily' # You didn't see yourself? Good job!
				else:
					continue # Patroller didn't see it, too bad
		
		if person == who: # Conjugate appropriately
			person = 'You'
		
		if stealth < 0:
			manner = ' blatantly' # This action was really obvious
		
		scount = p.no('success', row['success']) # Inflect for 2 successes, 1 success, no successes
		line = None
		
		if act == 'Feed':
			line = 'took {} blood on {}'.format(row['success'], row['detail'])
			bloodseen[row['detail']] += row['success']
		elif act == 'Patrol':
			line = 'patrolled with {}'.format(scount)
		elif act == 'Site Claim':
			line = 'claimed the {}'.format(row['detail'])
		elif act == 'Site Takeover':
			line = 'attacked the {} with {}'.format(row['detail'], scount)
		elif act == 'Site Defend':
			line = 'defended the {} with {}'.format(row['detail'], scount)
		elif act == 'Site Nuke':
			line = 'destroyed the {}!'.format(row['detail'])
		elif act == 'Site Audit':
			line = 'audited sites in the Regency'.format(row['actor'])
		elif act == 'Haven Hunt':
			line = 'searched for {}\'s Haven!'.format(row['detail'])
		elif act == 'Blood Attack':
			line = 'drove away {} blood'.format(row['success'])
		elif act == 'Blood Build':
			line = 'attracted {} blood'.format(row['success'])
		elif act == 'Custom':
			line = row['detail']
		else:
			continue # Ignore other actions
		
		planespec = ''
		if not exclusive: # Don't report this if it's evident
			if plane == 1:
				planespec = ' (in the Twilight)'
			elif plane == 2:
				planespec = ' (in the Shadow)'
		
		results.append(' - {}{} {}{}'.format(
			person,
			manner,
			line,
			planespec
		))
	
	if len(results) == 2: # All we saw was ourselves
		results[-1] = ' - No action apparent...'
	
#	total = db.c.execute("SELECT taken FROM Regencies WHERE name=?", (where,)).fetchone()[0]
#	results.append(' - {} blood seems to be missing...'.format(total - bloodseen))
	for date, seen in bloodseen.items():
		total = db.c.execute("SELECT SUM(success) FROM Actions WHERE type='Feed' AND location=? AND detail=?", (where, date)).fetchone()[0]
		if total is None: total = 0
		if total != seen:
			results.append(' - {} blood seems to be missing...'.format(total-seen))
			if len(bloodseen) > 1: results[-1] += ' (on {})'.format(date) # There are multiple dates here, need to be specific
	
	return results

def run():
	print('Beginning patrols...')
	patrols = []
	
	for row in db.c.execute("SELECT * FROM Actions WHERE type='Sheriff Patrol'"): # Convert Sheriff Patrols to normal Patrols
		sheriffpatrol(row['location'], row['actor'], row['success'], row['stealth'], row['detail'])
	
	for row in db.c.execute("SELECT * FROM Actions WHERE type='Patrol'"):
		patrols.append(patrol(row['location'], row['for'], row['success'], row['actor'], row['detail']))
	
	with open('out/Patrols.txt', 'w') as f:
		f.write(
			'\n\n'.join(
				'\n'.join(line for line in out)
			for out in patrols
		))
	print('Patrols complete')
