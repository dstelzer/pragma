import prettytable
import sqlite3

import db

def main():
	prompt = '>'
	buffer = str()
	
	while True:
		try:
			inp = input(prompt)
		except (EOFError, KeyboardInterrupt): # Control-C, Control-D
			break
		buffer += inp
		if buffer == 'quit' or buffer == 'exit' or buffer == 'quit;' or buffer == 'exit;':
			break
		if sqlite3.complete_statement(buffer):
			try:
				buffer = buffer.strip()
				db.cur.execute(buffer)
				tok = buffer.lstrip().upper()
				if tok.startswith('SELECT') or tok.startswith('PRAGMA'):
					print(prettytable.from_db_cursor(db.cur))
				elif tok.startswith('INSERT'):
					print('Insert complete.')
					print('Last row ID:', db.cur.lastrowid)
				else:
					print('Statement executed.')
				print('')
			except sqlite3.Error as e:
				print('ERROR: {}'.format(e))
			buffer = ''
			prompt = '>'
		else:
			prompt = '\t'