import csv

import db

def dump():
	with open('out/Expenditures.csv', 'w', newline='', encoding='utf-8') as f:
		out = csv.writer(f)
		
		out.writerow(('Kindred', 'Money', 'Vitae', 'Willpower'))
		
		for row in db.c.execute("SELECT for AS person, SUM(money) AS spentmoney, SUM(vitae) AS spentvitae, SUM(wp) AS spentwp FROM Actions GROUP BY person HAVING spentmoney>0 OR spentvitae>0 OR spentwp>0 ORDER BY person ASC"):
			out.writerow([i if i else '' for i in row])

def run():
	print('Writing expenses')
	dump()