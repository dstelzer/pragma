import db

def ischaracter(name):
	count = db.c.execute('SELECT COUNT(*) FROM Characters WHERE name=?', (name,)).fetchone()[0]
	return bool(count)

def regentof(where):
	regent = db.c.execute('SELECT regent FROM Regencies WHERE name=?', (where,)).fetchone()[0]
	return regent

def isregent(who, where):
	regent = regentof(where)
	return regent and regent == who

def position(who):
	return db.c.execute('SELECT position FROM Characters WHERE name=?', (who,)).fetchone()[0]

def isprince(who):
	return position(who) == 'Prince'

def issheriff(who):
	return position(who) == 'Sheriff'

def owner(what):
	own = db.c.execute('SELECT owner FROM Sites WHERE name=?', (what,)).fetchone()[0]
	return own

def owns(who, what):
	return who == owner(what)

def exists(what):
	count = db.c.execute('SELECT COUNT(*) FROM Sites WHERE name=?', (what,)).fetchone()[0]
	return bool(count)

def influence(what):
	inf = db.c.execute('SELECT influence FROM Sites WHERE name=?', (what,)).fetchone()[0]
	return inf