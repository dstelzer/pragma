import db
import csv
import os
import os.path

import big

def writetable(table):
	with open('csv/{}.csv'.format(table), 'w', newline='', encoding='utf-8') as f:
		write = csv.writer(f)
		
		db.cur.execute('SELECT * FROM {}'.format(table))
		write.writerow(i[0] for i in db.cur.description) # Headers
		write.writerows(db.cur)

def main():
	big.text('Export')
	
	tables = [row['name'] for row in db.c.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name")]
	
	if not os.path.isdir('csv'):
		os.mkdir('csv')
	
	for table in tables:
		print('Writing {} table...'.format(table))
		writetable(table)
	print('Done!')
	print('Now alter the files in /csv as you will.')

if __name__ == '__main__':
	main()
