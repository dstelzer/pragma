#!usr/bin/env python3.5

import sys
import os
import os.path

#import pyfiglet.fonts

import big

import main as mainprog
import makedb
import db2csv
import csv2db
import backup
import sqlshell
import bloodimg
import bloodwars

def blood_run():
	bloodimg.run()
	bloodwars.export()

options = (
	('Action Processing', mainprog.main),
	('Backup', backup.backup),
	('Restore', backup.restore),
	('Export to CSV', db2csv.main),
	('Import from CSV', csv2db.main),
	('SQL Shell', sqlshell.main),
	('Construct Database', makedb.main),
	('Draw Map', blood_run)
)

def exitfunc():
	sys.exit(0)

try:
	from cursesmenu import SelectionMenu
	select = SelectionMenu.get_selection
except ImportError: # No cursesmenu available
	def select(ops):
		for i, op in enumerate(ops):
			print('{} - {}'.format(i, op))
		print('{} - exit'.format(i+1))
		while True:
			try:
				val = int(input('Choose an option >'))
			except ValueError:
				val = -1
			
			if val >= 0 and val <= len(ops):
				return val
			else:
				print('Bad input')

def main():
	
	for dir in 'csv', 'data', 'out':
		if not os.path.isdir(dir):
			os.mkdir(dir)
	
	while True:
		act = select([op[0] for op in options])
		f = options[act][1] if act<len(options) else exitfunc
		f() # Call the function
		big.pause()

if __name__ == '__main__':
	main()
