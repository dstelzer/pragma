import shutil

import db
import big

def backup():
	big.text('Backup')
	shutil.copyfile('database.db', 'backup.db')
	print('Database copied to backup.db')

def restore():
	big.text('Restore')
	db.disconnect()
	shutil.copyfile('backup.db', 'database.db')
	db.reconnect()
	print('Restored from backup')